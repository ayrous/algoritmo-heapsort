package busca;

public class HeapTest {

	public static void main(String[] args) {
		
		int v[] = {4, 9, 6, 5, 3, 8, 15, 7, 18};
		imprime(v);
		heap(v);
		imprime(v);
		
		System.out.println("------------");
		
		int w[] = {5, 13, 8 , 9, 2 , 4, 23, 1, 3};
		imprime(w);
		heap(w);
		imprime(w);
		
	}
	
	public static void troca(int[] v, int i, int j) {
		int auxiliar = v[i];
		v[i]= v[j];
		v[j] = auxiliar;
	}
	
	public static void imprime(int[] v) {
		for(int i = 0; i < v.length; i++) {
			System.out.print(v[i] + " ");
		}
		System.out.println();
	}
	
	
	static void heapify(int v[], int n, int pai) {
		int max, filho;
		filho = 2 * pai + 1;
		max = pai;
		if (filho < n)
			if (v[filho] > v[max])
				max = filho;
		if (filho + 1 < n)
			if (v[filho + 1] > v[max])
				max = filho + 1;
		if (max != pai) {
			troca(v, max, pai);
			heapify(v, n, max);
		}
	}

	static void constroiHeap(int v[]) {
		for (int i = v.length / 2 - 1; i >= 0; i--)
			heapify(v, v.length, i);
	}

	static void heap(int v[]) {
		constroiHeap(v);
		for (int i = v.length - 1; i >= 1; i--) {
			troca(v, i, 0);
			heapify(v, i, 0);
		}
	}
}
